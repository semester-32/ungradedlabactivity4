//Rida Chaarani
//2137814

package linearalgebra;
import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests {
 
    @Test
    public void return_goodGets() {
        Vector3d vector = new Vector3d(5, 7, 9);
        assertEquals(5, vector.getX(), 0.001);
        assertEquals(7, vector.getY(), 0.001);
        assertEquals(9, vector.getZ(), 0.001);
    }

    @Test
    public void magnitudeTest_returnsGood() {
        Vector3d vector = new Vector3d(5, 7, 9);
        assertEquals(Math.sqrt(155), vector.magnitude(), 0.001);
    }
    
    @Test
    public void dotTest_returnGood() {
        Vector3d vector = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);

        assertEquals(32, vector.dotProduct(vector2), 0.001);
    }

    @Test
    public void addTest_returnGood() {
        Vector3d vector = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);

        Vector3d vectResult = vector.add(vector2);

        assertEquals(5, vectResult.getX(), 0.0001);
        assertEquals(7, vectResult.getY(), 0.0001);
        assertEquals(9, vectResult.getZ(), 0.0001);
    }

}
